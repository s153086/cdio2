package game;

public class cup {
	
private dice dice1;
private dice dice2;

public cup(){
	dice1 = new dice();
	dice2 = new dice();
	}

public void roll(){
	dice1.rolldice();
	dice2.rolldice();
	}

public int getSum(){
	return dice1.getFacevalue() + dice2.getFacevalue();
	}

public boolean getEns(){
	return dice1.compare(dice2);
	}

public int getdice1value(){
	return dice1.getFacevalue();
	}

public int getdice2value(){
	return dice2.getFacevalue();
	}
	
	
}

