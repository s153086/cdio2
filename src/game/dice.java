package game;

public class dice {

	private int facevalue;
	
	public dice(){
		facevalue = 0;
	}
	
	public int rolldice(){
		facevalue = (int) (1 + Math.random()*6);
		return facevalue; 
	}
	
	public boolean compare(dice otherdice){
		return facevalue == otherdice.facevalue;
	}
	
	public int getFacevalue(){
		return facevalue;
	}
}
