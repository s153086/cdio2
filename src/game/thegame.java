package game;

import desktop_resources.GUI;
import java.awt.Color;
import desktop_codebehind.Car;
import desktop_fields.*;

public class thegame {

	private final static int winnerbalance = 3000;
	private static cup cup;
	private static boolean running = true;
	
	
	public static void main(String[] args) {
		
		Field[] fields = new Field[12];
		fields[0] = new Street.Builder().setTitle("Start").setSubText(" ").build();
		fields[1] = new Street.Builder().setTitle("Tower").setSubText("+250").build();
		fields[2] = new Street.Builder().setTitle("Crater").setSubText("-100").build();
		fields[3] = new Street.Builder().setTitle("Palace Gates").setSubText("+100").build();
		fields[4] = new Street.Builder().setTitle("Cold Dessert").setSubText("-20").build();
		fields[5] = new Street.Builder().setTitle("Walled City").setSubText("+180").build();
		fields[6] = new Street.Builder().setTitle("Monastery").setSubText("0").build();
		fields[7] = new Street.Builder().setTitle("Black Cave").setSubText("-70").build();
		fields[8] = new Street.Builder().setTitle("Huts in the Mountain").setSubText("+60").build();
		fields[9] = new Street.Builder().setTitle("The Werewall").setSubText("-80, men 1 tur mere").build();
		fields[10] = new Street.Builder().setTitle("The Pit").setSubText("-50").build();
		fields[11] = new Street.Builder().setTitle("Goldmine").setSubText("+650").build();
		GUI.create(fields);
		
		Car carp1 = new Car.Builder().primaryColor(Color.blue).build();
		Car carp2 = new Car.Builder().primaryColor(Color.red).build();
		
		player p1 = new player(GUI.getUserString("Spiller 1 navn: "), carp1);
		player p2 = new player(GUI.getUserString("Spiller 1 navn: "), carp2);
		
		GUI.addPlayer(p1.getName(), p1.getBalance(), carp1);
		GUI.addPlayer(p2.getName(), p2.getBalance(), carp2);
		
		cup = new cup();
		
		GUI.showMessage("Vi er klar til at spiller, og " +p1.getName()+ " starter" );
		GUI.setCar(1, p1.getName());
		GUI.setCar(1, p2.getName());
	
		while (running){
			rollplayer(p1);
			
		if (running){
				rollplayer(p2);
				}
		else {
				GUI.showMessage("Spillet er slut");
				GUI.close();
				}
		}	
			
			}
		
		private static void rollplayer(player player){
			
			GUI.getUserButtonPressed("Sl� med terningerne", "Sl�");
			cup.roll();
			GUI.setDice(cup.getdice1value(), cup.getdice2value());
			GUI.showMessage(player.getName() + " sl�r : " + cup.getSum());
			
			
		if (player.getBalance() < winnerbalance){
			
		GUI.removeAllCars(player.getName());
		
			switch (cup.getSum())
			{
			case 2:
				GUI.showMessage("Du har fundet et t�rn og f�r 250 guld!");
				GUI.setCar(2, player.getName());
				player.addpoints(250);
				GUI.setBalance(player.getName(), player.getBalance());
				break;
			case 3:
				GUI.setCar(3, player.getName());
				GUI.showMessage("Du er faldet i et krater, og det koster 100 guld at f� dig op igen");
				player.removepoints(100);
				GUI.setBalance(player.getName(), player.getBalance());
				break;
			case 4:
				GUI.setCar(4, player.getName());
				GUI.showMessage("Du f�r en varm velkomst med 100 gold ved Palace Gates");
				player.addpoints(100);
				GUI.setBalance(player.getName(), player.getBalance());
				break;
			case 5:
				GUI.setCar(5, player.getName());
				GUI.showMessage("Cold Dessert er kold og du bruger 20 gold p� en ny hue");
				player.removepoints(20);
				GUI.setBalance(player.getName(), player.getBalance());
				break;
			case 6:
				GUI.setCar(6, player.getName());
				GUI.showMessage("Du f�r 180 gold for at kravle over v�ggene i Walled City");
				player.addpoints(180);
				GUI.setBalance(player.getName(), player.getBalance());
				break;
			case 7:
				GUI.setCar(7, player.getName());
				GUI.showMessage("Der sker vitterligt ingenting i Monastery...");
				GUI.setBalance(player.getName(), player.getBalance());
				break;
			case 8:
				GUI.setCar(8, player.getName());
				GUI.showMessage("Der er m�rkt i Black Cave og du bruger 70 gold p� en lygte");
				player.removepoints(70);
				GUI.setBalance(player.getName(), player.getBalance());
				break;
			case 9:
				GUI.setCar(9, player.getName());
				GUI.showMessage("Huts in the Mountain er stedet hvor 60 gold bare ligger og flyder");
				player.addpoints(60);
				GUI.setBalance(player.getName(), player.getBalance());
				break;
			case 10:
				GUI.setCar(10, player.getName());
				GUI.showMessage("The Werewall er et dyrt sted at v�re. Du betaler dog 80 gold for en ekstra tur");
				player.removepoints(80);
				GUI.setBalance(player.getName(), player.getBalance());
				rollplayer(player);
				break;
			case 11:
				GUI.setCar(11, player.getName());
				GUI.showMessage("Du er faldet i the Pit og det koster 50 for reb");
				player.removepoints(50);
				GUI.setBalance(player.getName(), player.getBalance());
				break;
			case 12:
				GUI.setCar(12, player.getName());
				GUI.showMessage("Tillykke! Tjener kassen p� denne Guldmine");
				player.addpoints(650);
				GUI.setBalance(player.getName(), player.getBalance());
				break;	
			}
			if (player.getBalance()>= winnerbalance){
				GUI.showMessage(player.getName() + " Har vundet, tillykke med det");
				running = false;
			}
			}
		else{
			GUI.showMessage(player.getName() + " Har vundet, tillykke med det");
		running = false;
		}
		}
	}
		
