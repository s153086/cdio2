package game;

import desktop_resources.*;
import desktop_codebehind.Car;
import desktop_fields.*;

public class player {

	private int balance = 1000;
	private String name;
	private int placement; // Hvor st�r spilleren p� br�ttet
	private Car car;
	
	public	player(String playername, Car car){
		name = playername;
		this.car = car;
		
	}

	public String getName(){
		return name;
	}
	
	public int getBalance(){
		return balance;
	}
	
	public void setBalance(int newbalance){
		balance = newbalance;
	}
	
	public void setPlacement(int newplacement){
		placement = newplacement;
	}
	
	public void addpoints(int points){
		balance += points;
	}
	
	public void removepoints(int points){
		balance = balance - points;
	}
	
	public String toString(){
		String s = ("" +name + " " + Integer.toString(balance)+ " points");
		return s;
	}


}